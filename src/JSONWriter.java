import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class JSONWriter {
	
	static String fileName = "/Users/agrawalr/Desktop/Golf data/data.xlsx";
	static ExcelReader doc = new ExcelReader();
	static StopWordFilter sw = new StopWordFilter();
	
	static ArrayList<HashMap<String,String>> result = doc.getMap(fileName);
	static Map<String, String> titleMap = new HashMap<>();
	static Map<String, String> idMap = new HashMap<>();
	
	public static void main(String args[]) {
		sw.readFiles();
		titleMap = result.get(0);
		idMap = result.get(1);
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootArray = mapper.readTree(new File("/Users/agrawalr/Desktop/Golf data/sample.json"));
			
			for(JsonNode root : rootArray) {
				JsonNode contentArray = root.path("content");
				
				for(JsonNode content : contentArray) {
					JsonNode type = content.path("type");
					if("Question".equals(type.asText())) {
						JsonNode items = content.path("items");
						JsonNode left = items.path("left");
						JsonNode right = items.findPath("right");
						
						String title = left.path("title").asText().split("\\|")[0].trim().toLowerCase();
						if(titleMap.containsKey(title)) {
							String tags[] = titleMap.get(title).split(",");							
							ArrayNode tagsNode = mapper.createArrayNode();
							for(String tag: tags) {
								tagsNode.add(tag.trim());
							}
							((ObjectNode) left).put("tags",tagsNode);
							titleMap.remove(title);
						}
						
						title = right.path("title").asText().split("\\|")[0].trim().toLowerCase();
						if(titleMap.containsKey(title)) {
							String tags[] = titleMap.get(title).split(",");							
							ArrayNode tagsNode = mapper.createArrayNode();
							for(String tag: tags) {
								tagsNode.add(tag.trim());
							}
							((ObjectNode) right).put("tags",tagsNode);
							titleMap.remove(title);
						}
					}
					
					if("problem".equals(type.asText())) {
						String title = content.path("title").asText().toLowerCase();
						String lessonId = content.path("lesson_id").asText();
						ArrayList<String> titles = new ArrayList<String>();
						for(String key: titleMap.keySet()) {
							if(partialMatch(title, key)) {
								URL jsonURL = new URL("http://www.golf.com/api/content/v1/videos?nid="+lessonId);
								JsonNode videoRoot = mapper.readTree(jsonURL);
								JsonNode data = videoRoot.path("data");
								JsonNode videoData = data.path("video_data");
								String videoId = videoData.path("video_id").asText();
								if(idMap.containsKey(videoId)) {
									String val = idMap.get(videoId);
									String tags[] = titleMap.get(val).split(",");							
									ArrayNode tagsNode = mapper.createArrayNode();
									for(String tag: tags) {
										tagsNode.add(tag.trim());
									}
									((ObjectNode) content).put("tags",tagsNode);
									titles.add(val);
								}
							}
						}
						for(String key : titles) {
							titleMap.remove(key);
						}
					}
				}
			}
			String resultUpdate = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootArray);
			System.out.println(resultUpdate);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean partialMatch(String str1, String str2) {
		if(str1.equals(str2)) return true;
		str1 = sw.eliminate(str1);
		StringTokenizer tok =new StringTokenizer(str1);
        while(tok.hasMoreTokens()){
            String s=tok.nextToken().toLowerCase();
            if(str2.contains(s)) return true;
        }
		return false;
	}
}