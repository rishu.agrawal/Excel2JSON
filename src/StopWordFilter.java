import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;
import java.util.ArrayList;

public class StopWordFilter {
	
	private ArrayList<String> stop_word = new ArrayList<>();
	private BufferedReader csv;
	
	public String eliminate(String text){
        StringTokenizer tok =new StringTokenizer(text);
        String b="";
        while(tok.hasMoreTokens()){
            String s=tok.nextToken().toLowerCase();
            if(!stop_word.contains(s)){
                b+=s+" ";
            }
        }
        b = b.trim();
        return b;
    }
	
	public void readFiles(){
        String line = "";
        String pathToSW = "/Users/agrawalr/Downloads/stop_words.txt";        
        
        try{ 
            csv = new BufferedReader(new FileReader(pathToSW));		
            while((line = csv.readLine()) != null){
                stop_word.add(line.trim());
            }
        }
        catch (Exception e) {e.printStackTrace();}   
    }
}
