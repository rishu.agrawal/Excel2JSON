import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

public class ExcelReader {
	
	private static XSSFWorkbook workbook;
	static HashMap<String, String> titleMap = new HashMap<String,String>();
	static HashMap<String, String> idMap = new HashMap<String, String>();
	
	private static String bcId, title, tags, type;

	public ArrayList<HashMap<String,String>> getMap(String fileName) {
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
	    try {
	        FileInputStream file = new FileInputStream(new File(fileName));
	        workbook = new XSSFWorkbook(file);
	        XSSFSheet sheet = workbook.getSheetAt(1);

	        Iterator<Row> rowIterator = sheet.iterator();
	        while (rowIterator.hasNext()) {
	        	int i=0;
	            Row row = rowIterator.next();
	            Iterator<Cell> cellIterator = row.cellIterator();
	            while (cellIterator.hasNext() && i<15) {
	            	i++;
	                Cell cell = cellIterator.next();
	                if(i==2) {
	                	if(cell.getCellTypeEnum() == CellType.STRING) {
	                		bcId = cell.getStringCellValue();
	                	} else {
	                		long id = (long) cell.getNumericCellValue();
	                		bcId = Long.toString(id);
	                	}
	                }
	                if(i==6) {
	                	title = cell.getStringCellValue().toLowerCase().trim();
	                }
	                if(i==8) {
	                	cell.getStringCellValue();
	                }
	                if(i==12) {
	                	type = cell.getStringCellValue();
	                }
	                if(i==15) {
	                	tags = cell.getStringCellValue();
	                }
	            }
	            if (type.equals("Lesson")) {
	            	idMap.put(bcId, title);
	            	titleMap.put(title,tags);
	            }
	        }
	        file.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    result.add(titleMap);
	    result.add(idMap);
	    return result;
	}
	
	public void printMap(Map<String, String> map) {
		for(String key : map.keySet()) {
    		System.out.println(key+" : "+map.get(key));
    	}
	}
}

